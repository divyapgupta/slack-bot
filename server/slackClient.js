'use strict';

const RtmClient = require('@slack/client').RtmClient;
const CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
const RTM_EVENTS = require('@slack/client').RTM_EVENTS;
let rtm = null;
let nlp = null;

function handleOnAuthenticated(rtmStartData) {
    console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}`);
}

function handleOnMessage(message) {
  console.log(message);
    if (message.channel === 'DCFH4GE7P' || message.channel === 'DCG468745') {
        nlp.ask(message.text, (err, res) => {
            if (err) {
                console.log(err);
                return;
            }

            try {
                console.log(res);
                if(res.bye) {
                        const greetings = require('./intents/greetingsIntent');
                        var response = "Bye, Have a great day!";
                        return rtm.sendMessage(response, message.channel);
                }
                if(res.greetings) {
                        const greetings = require('./intents/greetingsIntent');
                        var response = "Hello, how can I help you?";
                        return rtm.sendMessage(response, message.channel);
                }

                if(!res.intent || !res.intent[0] || !res.intent[0].value) {
                    throw new Error("Could not extract intent.")
                }

                const intent = require('./intents/' + res.intent[0].value + 'Intent');
                if(res.intent[0].value == 'Search') {
                  res.originalMessage = message.text;
                }
                intent.process(res, function(error, response) {
                    if(error) {
                        console.log(error.message);
                        return;
                    }

                    return rtm.sendMessage(response, message.channel);
                })

            } catch(err) {
                console.log(err);
                console.log(res);
                rtm.sendMessage("Sorry, I don't know what you are talking about! I can help you find: \n 1- Link of FSA's. eg. -> 'Link of Microservices' \n 2- Help you answer your query related to CADM. eg. -> 'Search What are the benefits of Microservices'", message.channel);
            }

        });
    }

}

function addAuthenticatedHandler(rtm, handler) {
    rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, handler);
}


module.exports.init = function slackClient(token, logLevel, nlpClient) {
    rtm = new RtmClient(token, { logLevel: logLevel });
    nlp = nlpClient;
    addAuthenticatedHandler(rtm, handleOnAuthenticated);
    rtm.on(RTM_EVENTS.MESSAGE, handleOnMessage);
    return rtm;
}

module.exports.addAuthenticatedHandler = addAuthenticatedHandler;
