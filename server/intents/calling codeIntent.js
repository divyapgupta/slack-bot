'use strict';

const request = require('superagent');

module.exports.process = function process(intentData, cb) {

    if(intentData.intent[0].value !== 'calling code')
        return cb(new Error(`Expected calling code intent, got ${intentData.intent[0].value}`));

    if(!intentData.location) return cb(new Error('Missing location in calling code intent'));

    const location = intentData.location[0].value.replace(/,.?iris/i, '');

    request.get(`https://restcountries.eu/rest/v2/name/${location}?fullText=true`, (err, res) => {
        if(err || res.statusCode != 200 || !res.body[0].callingCodes) {
            console.log(err);
            console.log(res.body);

            return cb(false, `I had a problem finding out the calling code of ${location}`);
        }

        return cb(false, `Calling code of ${location} is + ${res.body[0].callingCodes}`);
    });
}
