'use strict';

const request = require('superagent');

module.exports.process = function process(intentData, cb) {
  console.log(intentData);

    if(intentData.intent[0].value !== 'Principles')
        return cb(new Error(`I expected Principles intent, got ${intentData.intent[0].value}`));

    if(!intentData.search_query[0].value) return cb(new Error('Missing FSA'));
    console.log("here");
    // var searchQuery = intentData.originalMessage.replace("Search", '').replace("search", '');
    // var searchQuery = '';
    // intentData.search_query.forEach(function(element) {
    //     searchQuery += element.value+ ' ';
    //   });
    //   console.log(searchQuery);
    request.get(`http://testconfelb-864193337.eu-west-1.elb.amazonaws.com/rest/api/content/search?cql=(label%20=%20principle%20and%20type=page%20and%20space=CADM%20and%20label="${intentData.search_query[0].value}")`, (err, res) => {
        if(err || res.statusCode != 200 || !res.body.results[0] || !res.body.results[0]._links) {
            console.log(err);
            console.log(res.body);

            return cb(false, `I didn't find any principle for FSA: ${intentData.search_query[0].value}`);
        }
        console.log(res.body);
        var searchResponse = '';
        res.body.results.forEach(function(element) {
          searchResponse += `\n ${element.title} - ${res.body._links.base+element._links.tinyui}`;
        });
        return cb(false, `I found below principles for FSA - ${intentData.search_query[0].value} ${searchResponse}`);
    });
}
