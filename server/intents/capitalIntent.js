'use strict';

const request = require('superagent');

module.exports.process = function process(intentData, cb) {

    if(intentData.intent[0].value !== 'capital')
        return cb(new Error(`Expected capital intent, got ${intentData.intent[0].value}`));

    if(!intentData.location) return cb(new Error('Missing location in capital intent'));

    const location = intentData.location[0].value.replace(/,.?iris/i, '');

    request.get(`https://restcountries.eu/rest/v2/name/${location}?fullText=true`, (err, res) => {
        if(err || res.statusCode != 200 || !res.body[0].capital) {
            console.log(err);
            console.log(res.body);

            return cb(false, `I had a problem finding out the capital of ${location}`);
        }

        return cb(false, `Capital of ${location} is ${res.body[0].capital}`);
    });
}
