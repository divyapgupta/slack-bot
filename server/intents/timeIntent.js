'use strict';

const request = require('superagent');

module.exports.process = function process(intentData, cb) {

    if(intentData.intent[0].value !== 'time')
        return cb(new Error(`Expected time intent, got ${intentData.intent[0].value}`));

    if(!intentData.location) return cb(new Error('Missing location in time intent'));

    const location = intentData.location[0].value.replace(/,.?iris/i, '');

    request.get(`http://api.timezonedb.com/v2/get-time-zone?key=UZE3WTVB9DSQ&format=json&by=zone&zone=America/Chicago`, (err, res) => {
        if(err || res.statusCode != 200 || !res.body.formatted) {
            console.log(err);
            console.log(res.body);

            return cb(false, `I had a problem finding out the time in ${location}`);
        }

        return cb(false, `In ${location}, it is now ${res.body.formatted}`);
    });
}
