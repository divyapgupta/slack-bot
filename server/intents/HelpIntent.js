'use strict';

const request = require('superagent');

module.exports.process = function process(intentData, cb) {

    if(intentData.intent[0].value !== 'Help') {
        return cb(new Error(`Expected Help intent, got ${intentData.intent[0].value}`));
      }
        return cb(false, "Hi There, need help? I can help you find: \n 1- Link of FSA's. eg. -> 'Link of Microservices' \n 2- Help you answer your query related to CADM. eg. -> 'Search What are the benefits of Microservices'");
    }
