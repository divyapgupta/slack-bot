'use strict';

const slackClient = require('../server/slackClient');
const service = require('../server/service');
const http = require('http');
const server = http.createServer(service);

const witToken = 'LQ3WSBVVJJHX7PAD5OO7HYIBWYXM4URQ';
const witClient = require('../server/witClient')(witToken);

const slackToken = 'xoxb-424771791958-423433934994-gYxkLefB38lkBZkILIwlqVNc';
const slackLogLevel = 'verbose';

const rtm = slackClient.init(slackToken, slackLogLevel, witClient);
rtm.start();

slackClient.addAuthenticatedHandler(rtm, () => server.listen(3000));

server.on('listening', function() {
    console.log(`IRIS is listening on ${server.address().port} in ${service.get('env')} mode.`);
});
