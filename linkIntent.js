'use strict';

const request = require('superagent');

module.exports.process = function process(intentData, cb) {

    if(intentData.intent[0].value !== 'link')
        return cb(new Error(`I expected LINK intent, got ${intentData.intent[0].value}`));

    if(!intentData.search_query[0].value) return cb(new Error('Did you miss the FSA name'));
    console.log("here");

    request.get(`http://testconfelb-864193337.eu-west-1.elb.amazonaws.com/rest/api/content/search?cql=(type=page%20and%20space=CADM%20and%20text~${intentData.search_query[0].value}%20and%20label=fsa)`, (err, res) => {
        if(err || res.statusCode != 200 || !res.body.results[0] || !res.body.results[0]._links) {
            console.log(err);
            console.log(res.body);

            return cb(false, `I had a problem finding out the link of FSA ${intentData.search_query[0].value}`);
        }
        console.log(res.body.results[0]._links);
        return cb(false, `I found below link of FSA ${intentData.search_query[0].value}: \n ${res.body._links.base+res.body.results[0]._links.tinyui}`);
    });
}
